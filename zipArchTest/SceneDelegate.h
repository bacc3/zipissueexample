//
//  SceneDelegate.h
//  zipArchTest
//
//  Created by Vasiliy Korchagin on 26.06.2020.
//  Copyright © 2020 Vasiliy Korchagin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

