//
//  ViewController.m
//  zipArchTest
//
//  Created by Vasiliy Korchagin on 26.06.2020.
//  Copyright © 2020 Vasiliy Korchagin. All rights reserved.
//

#import "ViewController.h"
#import <SSZipArchive/SSZipArchive.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *dirToUnzip = [self dirToUnzip];
    [SSZipArchive unzipFileAtPath:[self zipFilePath]
                    toDestination:dirToUnzip];
    NSLog(@"Files unzipped to %@", dirToUnzip);
}

#pragma mark - Private

- (NSString *)zipFilePath {
    
    return [[NSBundle mainBundle] pathForResource:@"test" ofType:@"zip"];
}

- (NSString *)dirToUnzip {
    
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"unzipped"];
}

@end
